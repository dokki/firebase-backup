import firebase_admin
from firebase_admin import credentials
from firebase_admin import _utils
from firebase_admin import auth
import json
from datetime import datetime
import sys 
import base64
import os
import logging
import boto3
from botocore.exceptions import ClientError
from dotenv import load_dotenv
load_dotenv()

def exportUsersAsJson(file_name = None):
    cred = credentials.Certificate( os.path.dirname(os.path.realpath(__file__)) +  '/firebase_key.json')

    default_app = firebase_admin.initialize_app(cred)

    page = auth.list_users()

    data = {}
    data['users'] = []

    while page:
        for user in page.users:
            pwd = user.password_hash
            salt = user.password_salt
            pwd = pwd.replace('_', '/')
            pwd = pwd.replace('-', '+')

            salt = salt.replace('_', '/')
            salt = salt.replace('-', '+')

            data['users'].append({
                'localId' : user._data['localId'],
                'email' :  user._data['email'],
                'emailVerified' : user._data['emailVerified'],
                'passwordHash' :  pwd,
                'salt' : salt,
                'lastSignedInAt' : user._data['lastLoginAt']  if ('lastLoginAt' in user._data) else '',
                'createdAt' : user._data['createdAt'],
                'disabled' : user._data['disabled'],
                'providerUserInfo' : []
            })
        page = page.get_next_page()

    today = datetime.now()

    if(file_name == None):
        file_name = 'backup-'+ today.strftime("%m-%d-%Y_%H:%M:%S")+ '.json'

    with open(file_name, 'w') as outfile:
        json.dump(data, outfile)
    
    return file_name

def uploadFileOnS3(file_name, delete):
    s3 = boto3.client('s3')
    with open(file_name, "rb") as f:
        s3.upload_fileobj(f, "prod-firebase-auth-backup", file_name)

    if(delete):
        os.remove(file_name)

    print("File " + file_name + " saved")

if(sys.argv[1] == 's3'):
    file_name = exportUsersAsJson()
    uploadFileOnS3(file_name, True)

if(sys.argv[1] == 'local'):
    file_name = exportUsersAsJson("export.json")